﻿
anb_game_start = {
	effect = {
		set_immortal_age_for_scripted_characters = yes
		
		setup_racial_traits_for_all_characters = yes
		racial_lifestyles_setup = yes	#linked to traits above

		anb_set_culture_descriptions_effect = yes

		every_living_character = {
			limit = {
				has_culture = culture:pearlsedger
			}
			if = {
				limit = {
					NOT = {
						has_character_flag = bald_head
					}
					OR = {
						trigger_if = {
							limit = { is_female = no }
							is_married = yes
						}
						trigger_if = {
							limit = { is_female = yes }
							martial >= 30
						}
						trigger_else = {
							martial >= 20
						}
						trigger_if = {
							limit = { is_female = yes }
							prowess >= 30
						}
						trigger_else = {
							prowess >= 20
						}
					}
				}
				add_character_flag = {
					flag = bald_head
					days = -1
				}
				add_prestige = 10
			}
			if = {
				limit = { has_trait = lifestyle_blademaster_3_history }
				remove_trait = lifestyle_blademaster_3_history
				add_trait = lifestyle_blademaster
				add_trait_xp = {
					trait = lifestyle_blademaster
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_blademaster_2_history }
				remove_trait = lifestyle_blademaster_2_history
				add_trait = lifestyle_blademaster
				add_trait_xp = {
					trait = lifestyle_blademaster
					value = 50
				}
			}
			if = {
				limit = { has_trait = lifestyle_mystic_3_history }
				remove_trait = lifestyle_mystic_3_history
				add_trait = lifestyle_mystic
				add_trait_xp = {
					trait = lifestyle_mystic
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_mystic_2_history }
				remove_trait = lifestyle_mystic_2_history
				add_trait = lifestyle_mystic
				add_trait_xp = {
					trait = lifestyle_mystic
					value = 50
				}
			}
			if = {
				limit = { has_trait = lifestyle_physician_3_history }
				remove_trait = lifestyle_physician_3_history
				add_trait = lifestyle_physician
				add_trait_xp = {
					trait = lifestyle_physician
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_physician_2_history }
				remove_trait = lifestyle_physician_2_history
				add_trait = lifestyle_physician
				add_trait_xp = {
					trait = lifestyle_physician
					value = 50
				}
			}
			if = {
				limit = { has_trait = lifestyle_reveler_3_history }
				remove_trait = lifestyle_reveler_3_history
				add_trait = lifestyle_reveler
				add_trait_xp = {
					trait = lifestyle_reveler
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_reveler_2_history }
				remove_trait = lifestyle_reveler_2_history
				add_trait = lifestyle_reveler
				add_trait_xp = {
					trait = lifestyle_reveler
					value = 50
				}
			}
			if = {
				limit = { has_trait = lifestyle_hunter_3_history }
				remove_trait = lifestyle_hunter_3_history
				add_trait = lifestyle_hunter
				add_trait_xp = {
					trait = lifestyle_hunter
					track = hunter
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_hunter_2_history }
				remove_trait = lifestyle_hunter_2_history
				add_trait = lifestyle_hunter
				add_trait_xp = {
					trait = lifestyle_hunter
					track = hunter
					value = 50
				}
			}
			if = {
				limit = { has_trait = lifestyle_falconer_3_history }
				remove_trait = lifestyle_falconer_3_history
				add_trait = lifestyle_hunter
				add_trait_xp = {
					trait = lifestyle_hunter
					track = falconer
					value = 100
				}
			}
			if = {
				limit = { has_trait = lifestyle_falconer_2_history }
				remove_trait = lifestyle_falconer_2_history
				add_trait = lifestyle_hunter
				add_trait_xp = {
					trait = lifestyle_hunter
					track = falconer
					value = 50
				}
			}
		}
		
		anb_setup_coa_game_start_effect = yes
		
		mark_faiths_to_found = yes
		
		# Setup bilingualism
		anb_setup_bilingual_traditions_game_start_effect = yes
		
		# Regents/Viziers
		anb_setup_diarchies_game_start_effect = yes
		
		# LAAMP SETUP
		anb_setup_laamp_game_start_effect = yes
	}
}

anb_on_game_start_after_lobby = {
	events = {
		anb_harpy_initialization.0001 #Get rid of male "harpies"
	}
	effect = {
		#Anbennar - Setup Racial Purity
		if = {
			limit = { has_game_rule = legitimacy_and_supremacy_on }
			#Add racial attitude to characters
			every_living_character = {
				limit = { NOT = { has_variable = no_purist_trait } } # Don't give it to characters marked as not racist
				roll_racial_purity_trait_for_character = yes
			}
		}
		else = {
			every_living_character = {
				remove_racial_purist_traits = yes
			}
		}

		if = {
			limit = {
				OR = {
					has_game_rule = starting_randomized_races
					has_game_rule = all_randomized_races
				}
			}
			every_living_character = {
				set_random_racial_trait = yes
			}
		}
	}
}
