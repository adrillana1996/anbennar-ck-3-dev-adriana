﻿##########
# Calasandur's Elven Castles
##########

calasandur_castle_aelcandar_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_alhambra.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.2
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.15
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

calasandur_castle_escandar_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_alhambra.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.2
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.15
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

calasandur_castle_calascandar_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_alhambra.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.3
	}
	max_garrison = good_building_max_garrison_tier_4
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_4
		fort_level = good_building_fort_level_tier_4
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.25
		monthly_county_control_growth_add = 1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

##########
# Castanorian Citadels
##########

castanorian_citadel_bal_ouord_01 = { #Ruins
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	max_garrison = good_building_max_garrison_tier_1
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_1
		fort_level = good_building_fort_level_tier_1
	}
	
	next_building = castanorian_citadel_bal_ouord_02
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_ouord_02 = { #Rebuilt
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_dostan_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_vertesk_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_vroren_01 = { #Ruins
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	max_garrison = good_building_max_garrison_tier_1
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_1
		fort_level = good_building_fort_level_tier_1
	}
	
	next_building = castanorian_citadel_bal_vroren_02
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_vroren_02 = { #Rebuilt
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_hyl_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_mire_01 = { #Ruins
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	max_garrison = good_building_max_garrison_tier_1
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_1
		fort_level = good_building_fort_level_tier_1
	}
	
	next_building = castanorian_citadel_bal_mire_02
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_bal_mire_02 = { #Rebuilt
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.1
	}
	max_garrison = good_building_max_garrison_tier_3
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_3
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_north_citadel_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.3
	}
	max_garrison = good_building_max_garrison_tier_4
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_4
		fort_level = good_building_fort_level_tier_4
	}
	county_modifier = {
		development_growth = 0.1
		development_growth_factor = 0.1
		monthly_county_control_growth_add = 1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

castanorian_citadel_south_citadel_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_citadel_of_aleppo.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_hoardings
		}
	}
	
	cost_gold = 2000

	character_modifier = {
		monthly_prestige = 0.3
		monthly_prestige_gain_mult = 0.05
	}
	max_garrison = good_building_max_garrison_tier_4
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_4
		fort_level = good_building_fort_level_tier_4
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.10
		monthly_county_control_growth_add = 1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

##########
# Balgar's Wonders
##########

white_walls_of_castanor_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_hadrians_wall.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000

	max_garrison = good_building_max_garrison_tier_1
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_1
		fort_level = good_building_fort_level_tier_1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

dragonforge_01 = {
	construction_time = very_slow_construction_time
	
	type_icon = "icon_building_blacksmiths.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_royal_armory
		}
	}
	
	cost_gold = 3000
	
	character_modifier = {
		monthly_prestige = 0.3
		prowess_per_prestige_level = 2
		pikemen_damage_mult = 0.1
		pikemen_toughness_mult = 0.3
		heavy_infantry_damage_mult = 0.1
		heavy_infantry_toughness_mult = 0.3
		men_at_arms_maintenance = -0.1
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.25
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

##########
# Bulwar Waterworks
##########

bulwar_waterworks_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_tradeport.dds"

	can_construct_potential = {
		barony = title:b_bulwar
		building_requirement_tribal = no
	}

	cost_gold = 3000
	
	duchy_capital_county_modifier = {
		tax_mult = 0.15
	}
	county_modifier = {
		supply_limit_mult = 0.5
		development_growth = 0.3
		development_growth_factor = 0.5
	}
	character_modifier = {
		stewardship_per_prestige_level = 1
	}
	
	#next_building = common_tradeport_02 #i have no idea why this is here, it breaks stuff
	
	ai_value = {
		base = 10
	}
	
	type = duchy_capital
}

##########
# Ekluzagnu
##########

ekluzagnu_01 = {
	asset = {
		type = pdxmesh
		name = "building_special_petra_mesh"
	}

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_petra.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000

	county_modifier = {
		tax_mult = 0.1
		supply_limit_mult = 0.25
		development_growth = 0.1
		development_growth_factor = 0.15
	}
	
	province_modifier = {
		monthly_income = good_building_tax_tier_2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

##########
# Oldpassage
##########

oldpassage_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_mines.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	province_modifier = {
		monthly_income = 2
		defender_holding_advantage = normal_building_advantage_tier_1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

##########
# The Necropolis
##########

holy_site_the_necropolis_01 = {
	
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_mahabodhi_temple.dds"
	
	can_construct = {
		OR = {
			scope:holder = {
				religion = religion:cannorian_pantheon_religion
			}
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
		}
		# scope:holder = {
		# 	culture = {
		# 		has_innovation = innovation_crop_rotation
		# 	}
		# }
	}
	
	is_enabled = {
		custom_description = {
			text = holy_site_pantheonic_or_holy_site_trigger
			OR = {
				scope:holder = {
					religion = religion:cannorian_pantheon_religion
				}
				custom_description = {
					text = holy_site_building_trigger
					barony = {
						is_holy_site_of = scope:holder.faith
					}
				}
			}
		}
	}
	
	show_disabled = yes
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 1
		monthly_dynasty_prestige_mult = 0.05
		learning_per_piety_level = 2
		different_faith_opinion = 5
		religious_vassal_opinion = 10
		different_faith_county_opinion_mult = -0.2
	}
	
	county_modifier = {
		tax_mult = 0.2
		development_growth_factor = 0.3
		development_growth = 0.2
	}
	
	province_modifier = {
		monthly_income = 3
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

########
# Marrhold Griffongate
########

griffon_gate_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_hadrians_wall.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000

	max_garrison = good_building_max_garrison_tier_2
	province_modifier = {
		defender_holding_advantage = normal_building_advantage_tier_3
		fort_level = good_building_fort_level_tier_1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

########
# Mother's Sanctuary
########
holy_site_mothers_sanctuary_01 = {

	asset = {
		type = pdxmesh
		name = "building_special_cathedral_generic_mesh"
	}

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_cologne_cathedral.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		OR = {
			scope:holder = {
				religion = religion:cannorian_pantheon_religion
			}
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
		}
		scope:holder = {
			culture = {
				has_innovation = innovation_guilds
			}
		}
	}
	
	is_enabled = {
		custom_description = {
			text = holy_site_pantheonic_or_holy_site_trigger
			OR = {
				scope:holder = {
					religion = religion:cannorian_pantheon_religion
				}
				custom_description = {
					text = holy_site_building_trigger
					barony = {
						is_holy_site_of = scope:holder.faith
					}
				}
			}
		}
	}
	
	show_disabled = yes
	
	cost_gold = 1000

	province_modifier = {
		monthly_income = 3
	}

	county_modifier = {
		tax_mult = 0.20
		development_growth_factor = 0.3
		development_growth = 0.5
		county_opinion_add = 10
	}

	character_modifier = {
		monthly_piety = 0.5
		health = 0.5
		years_of_fertility = 10
		fertility = 0.3
		diplomacy_per_piety_level = 1
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_religious
}

########
# Lake Palace
########

lake_palace_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_doges_palace.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 500

	province_modifier = {
		fort_level = good_building_fort_level_tier_1
		hostile_raid_time = 1
	}

	county_modifier = {
		monthly_county_control_growth_add = 0.5
		development_growth_factor = 0.1
	}

	character_modifier = {
		embarkation_cost_mult = -0.2
		naval_movement_speed_mult = 0.25
	}
	
	next_building = lake_palace_02
	
	ai_value = {
		base = 100
		culture_likely_to_fortify_modifier = yes
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special
}

lake_palace_02 = {
	
	construction_time = slow_construction_time

	type_icon = "icon_structure_doges_palace.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder.culture = {
			has_innovation = innovation_heraldry
		}
	}
	
	cost_gold = 500

	province_modifier = {
		fort_level = 1
		hostile_raid_time = 0.5
	}

	county_modifier = {
		monthly_county_control_growth_add = 0.5
		development_growth_factor = 0.1
	}

	character_modifier = {
		embarkation_cost_mult = -0.1
		naval_movement_speed_mult = 0.1
		monthly_prestige_gain_per_happy_powerful_vassal_add = 0.15
		legitimacy_gain_mult = 0.1
		monthly_dynasty_prestige_mult = 0.1
		courtly_opinion = 5
		powerful_vassal_opinion = 5
		parochial_opinion = 5
	}
	
	ai_value = {
		base = 100
		culture_likely_to_fortify_modifier = yes
	}
	
	type = special

	flag = travel_point_of_interest_diplomatic
}

########
# Universities
########

konwell_university = {
	construction_time = very_slow_construction_time

	effect_desc = {
		desc = unlocks_building_desc
		triggered_desc = {
			trigger = { has_dlc_feature = tours_and_tournaments }
			desc = university_toto_effect_desc
		}
		triggered_desc = {
			trigger = { has_dlc_feature = royal_court }
			desc = university_roco_effect_desc
		}
		desc = university_effect_desc
	}

	type_icon = "icon_structure_university_of_siena.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
		building_university_requirement = yes
	}
	
	can_construct = {
	}
	
	show_disabled = yes
	
	cost_gold = 1000

	character_modifier = {
		learning_per_prestige_level = 1
		stewardship_per_prestige_level = 1
		monthly_lifestyle_xp_gain_mult = 0.1
		cultural_head_fascination_mult = 0.05
		monthly_dynasty_prestige = 0.25
	}
	
	county_modifier = {
		development_growth_factor = 0.2
		development_growth = 0.3
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_learning
}

silmuna_university = {
	construction_time = very_slow_construction_time

	effect_desc = {
		desc = unlocks_building_desc
		triggered_desc = {
			trigger = { has_dlc_feature = tours_and_tournaments }
			desc = university_toto_effect_desc
		}
		triggered_desc = {
			trigger = { has_dlc_feature = royal_court }
			desc = university_roco_effect_desc
		}
		desc = university_effect_desc
	}

	type_icon = "icon_structure_university_of_siena.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
		building_university_requirement = yes
	}
	
	can_construct = {
	}
	
	show_disabled = yes
	
	cost_gold = 1000

	character_modifier = {
		learning_per_prestige_level = 1
		# Something Magical
		monthly_lifestyle_xp_gain_mult = 0.1
		cultural_head_fascination_mult = 0.05
		monthly_dynasty_prestige = 0.25
	}
	
	county_modifier = {
		development_growth_factor = 0.2
		development_growth = 0.3
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_learning
}