﻿cliff_gnomish = {
	color = { 245 161 216 }

	ethos = ethos_stoic
	heritage = heritage_gnomish
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_philosopher_culture
		tradition_fishermen
	}
	
	name_list = name_list_gnomish
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

    ethnicities = {
        100 = gnome
    }
}

creek_gnomish = {
	color = "iochand_green"

	ethos = ethos_bureaucratic
	heritage = heritage_gnomish
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_life_is_just_a_joke
		tradition_ruling_caste
	}
	dlc_tradition = {
		trait = tradition_music_theory
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_gnomish
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

    ethnicities = {
        100 = gnome
    }
}

imperial_gnomish = {
	color = { 210 62 230 }

	ethos = ethos_bureaucratic
	heritage = heritage_gnomish
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_philosopher_culture
	}
	
	name_list = name_list_gnomish
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

    ethnicities = {
        100 = gnome
    }
}

delta_gnomish = {
	color = { 90 170 150 }
	created = 335.1.1
	parents = { creek_gnomish }

	ethos = ethos_bureaucratic
	heritage = heritage_gnomish
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_philosopher_culture
		tradition_wetlanders
		tradition_loyal_soldiers
	}
	
	name_list = name_list_gnomish
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

    ethnicities = {
        100 = gnome
    }
}