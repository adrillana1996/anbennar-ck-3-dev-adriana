### HUMANS

### ELVES

### WOOD-ELVES

### DWARVES

### KOBOLDS

### HARPIES

### HALFLINGS

slingers = {
	type = archers

	damage = 20
	toughness = 10
	pursuit = 0
	screen = 0

	terrain_bonus = {
		hills = { damage = 10 toughness = 8 }
		forest = { damage = 10 toughness = 8 }
		farmlands = { damage = 10 toughness = 8 }
		taiga = { damage = 10 toughness = 8}
	}

	counters = {
		skirmishers = 1
	}

	winter_bonus = {

	}
	can_recruit = {
		AND = {
			culture = {
				has_cultural_pillar = heritage_halfling
			}
			NOT = {
				culture = {
					has_cultural_parameter = unlock_maa_anb_oakfoot_rangers
			   }
			}
		}
	}
	
	buy_cost = { gold = slingers_recruitment_cost }
	low_maintenance_cost = { gold = slingers_low_maint_cost }
	high_maintenance_cost = { gold = slingers_high_maint_cost }
	
	stack = 150
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = archers
}

small_militia = {
	type = skirmishers

	damage = 10
	toughness = 10
	pursuit = 5
	screen = 20

	terrain_bonus = {
		hills = { damage = 10 toughness = 8 }
		forest = { damage = 10 toughness = 8 }
		farmlands = { damage = 10 toughness = 8 }
		wetlands = { damage = 10 toughness = 8 }
	}

	counters = {
		heavy_infantry = 1
		heavy_cavalry = 1
	}

	winter_bonus = {

	}
	can_recruit = {
		culture = {
			has_cultural_pillar = heritage_halfling
		}
	}
	
	buy_cost = { gold = small_militia_recruitment_cost }
	low_maintenance_cost = { gold = small_militia_low_maint_cost }
	high_maintenance_cost = { gold = small_militia_high_maint_cost }
	
	stack = 200
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = skirmishers
}

pony_riders = {
	type = light_cavalry

	damage = 20
	toughness = 17
	pursuit = 20
	screen = 20

	terrain_bonus = {
		plains = { damage = 10 toughness = 8 }
		drylands = { damage = 10 toughness = 8 }
		farmlands = { damage = 10 toughness = 8 }
		wetlands = { damage = -5 toughness = -4 }
		hills = { damage = -5 toughness = -4 }
		mountains = { damage = -5 toughness = -4 }
		desert_mountains = { damage = -5 toughness = -4 }
	}

	counters = {
		archers = 1
	}

	winter_bonus = {
		normal_winter = { damage = -5 toughness = -5 }
		harsh_winter = { damage = -5 toughness = -5 }
	}
	can_recruit = {
		culture = {
			has_cultural_pillar = heritage_halfling
		}
	}
	
	buy_cost = { gold = pony_riders_recruitment_cost }
	low_maintenance_cost = { gold = pony_riders_low_maint_cost }
	high_maintenance_cost = { gold = pony_riders_high_maint_cost }
	
	stack = 100
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = light_cavalry
}

borderwatch = {
	type = pikemen

	damage = 18
	toughness = 18
	pursuit = 0
	screen = 0

	terrain_bonus = {
		hills = { damage = 10 toughness = 8 }
		farmlands = { damage = 10 toughness = 8 }
	}

	counters = {
		light_cavalry = 1
		heavy_cavalry = 1
		elephant_cavalry = 1
	}

	winter_bonus = {
		
	}
	can_recruit = {
		culture = {
			has_cultural_pillar = heritage_halfling
		}
	}
	
	buy_cost = { gold = borderwatch_recruitment_cost }
	low_maintenance_cost = { gold = borderwatch_low_maint_cost }
	high_maintenance_cost = { gold = borderwatch_high_maint_cost }
	
	stack = 150
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = pikemen
}

### GNOMES