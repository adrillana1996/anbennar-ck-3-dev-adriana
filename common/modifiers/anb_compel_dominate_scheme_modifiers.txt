﻿### Compel & Dominate ###

compel_personal_insights = {
	icon = social_positive
	scheme_success_chance = 10
	scheme_phase_duration = miniscule_scheme_phase_duration_bonus_value
}

compel_personal_insights_direct = {
	icon = social_positive
	scheme_success_chance = 10
	scheme_phase_duration = miniscule_scheme_phase_duration_bonus_value
	scheme_secrecy = -10
}

compel_personal_insights_strong = {
	icon = social_positive
	scheme_success_chance = 20
	scheme_phase_duration = minor_scheme_phase_duration_bonus_value
}

dominate_opposing_presence = {
	icon = magic_negativ
	scheme_success_chance = -20
	scheme_phase_duration = minor_scheme_phase_duration_malus_value
	scheme_secrecy = 10
}