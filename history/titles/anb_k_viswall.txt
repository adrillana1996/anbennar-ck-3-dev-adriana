k_viswall = {
	1000.1.1 = {
		change_development_level = 8
	}
	1021.10.3 = {
		holder = sil_vis_0001 #Finnic sil Vis
	}
}

d_viswall = {
	970.1.1 = {
		holder = hill_0002 # King Henric
	}
	992.1.1 = {
		holder = hill_0003 # Lord Bobbin Hill
	}
	994.1.1 {
		holder = 724 # Mafalda the Mad
	}
	1021.10.3 = {
		holder = sil_vis_0001 #Finnic sil Vis
	}
}

c_viswall = {
	1000.1.1 = {
		change_development_level = 20
	}
	970.1.1 = {
		holder = hill_0002 # King Henric
	}
	992.1.1 = {
		holder = hill_0003 # Lord Bobbin Hill
	}
	994.1.1 {
		holder = 724 # Mafalda the Mad
	}
	1021.10.3 = {
		holder = sil_vis_0001 #Finnic sil Vis
	}
}

b_north_viswall = {
	1021.1.1 = {
		holder = hill_0012 # Torbin Hill, Baron of North Viswall
	}
}

b_south_viswall = {
	1021.1.1 = {
		holder = hill_0010 # Sam Hill, Baron of South Viswall
	}
}

d_barrowshire = {
	1000.1.1 = {
		change_development_level = 10
	}
	970.1.1 = {
		holder = hill_0002 # King Henric
	}
	1021.10.3 = {
		holder = sil_vis_0001 #Finnic sil Vis
	}
}

c_barrowshire = {
	1000.1.1 = {
		change_development_level = 11
	}
	970.1.1 = {
		holder = barrows_0001 # Lord Toman Barrows
	}
	1022.1.1 = {
		liege = d_barrowshire
	}
}

c_coppertown = {
	961.1.1 = {
		holder = copperburn_0001 # Lord Cooper Copperburn
	}
	990.1.1 = {
		holder = copperburn_0002 # Lord Humbard Copperburn
	}
	1022.1.1 = {
		liege = d_barrowshire
	}
}

c_hillwater = {
	825.1.1 = {
		holder = hill_0001 # Grif Hill
	}
	970.1.1 = {
		holder = hill_0002 # King Henric
	}
	992.1.1 = {
		holder = hill_0003 # Lord Bobbin Hill
	}
	994.1.1 = {
		holder = hill_0005 # Donna Hill
	}
	1022.1.1 = {
		liege = d_barrowshire
	}
}

c_aelcandar = {
	1007.1.1 = {
		holder = smallshield_0001 # Lord Celodorian Smallshield
	}
	1022.1.1 = {
		liege = d_barrowshire
	}
}

#d_greymill = {}

c_greymill = {
	982.1.1 = {
		holder = grey_0001 # Lord Ardor Grey
	}
	990.1.1 = {
		holder = grey_0009 # Lord Vincen
	}
	1022.1.1 = {
		liege = k_viswall
	}
}

c_norley = {
	1000.1.1 = {
		change_development_level = 7
	}
	1021.12.1 = {
		holder = norley_0001 # Lord Lan Norley
		liege = k_viswall
	}
}

c_old_course = {
	1000.1.1 = {
		change_development_level = 7
	}
	982.1.1 = {
		holder = grey_0001 # Lord Ardor Grey
	}
	990.1.1 = {
		holder = grey_0009 # Lord Vincen
	}
	1021.12.1 = {
		holder = courseheir_0001 # Lord Lombar Courseheir
		liege = k_viswall
	}
}

#d_thomsbridge = {}

c_thomsbridge ={
	1000.1.1 = {
		change_development_level = 9
	}
	930.1.1 = {
		holder = thomsbridge_0001 # Lord Thom "the Bridgewright"
	}
	970.1.1 = {
		holder = thomsbridge_0002 # Lord Thom “the Tollman”
	}
	1000.1.1 = {
		holder = thomsbridge_0005 # Lord Thom “the Bridgekeep”
	}
	1022.1.1 = {
		liege = k_viswall
	}
}

c_merryfield = {
	930.1.1 = {
		holder = thomsbridge_0001 # Lord Thom "the Bridgewright"
	}
	970.1.1 = {
		holder = thomsbridge_0002 # Lord Thom “the Tollman”
	}
	1000.1.1 = {
		holder = thomsbridge_0005 # Lord Thom “the Bridgekeep”
	}
	1022.1.1 = {
		liege = k_viswall
	}
}

c_tipney = {
	970.1.1 = {
		holder = tip_0002 # Dabbo Tip
	}
	1010.1.1 = {
		holder = tip_0003 # Lord Crovan Tip
	}
	1022.1.1 = {
		liege = k_viswall
	}
}