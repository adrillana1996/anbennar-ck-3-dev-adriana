k_merewood = {
	1000.1.1 = { change_development_level = 8 }
}

d_merescker = { 
	1021.9.30 = {
		holder = merewood_0001 # Edmund Fairhand
	}
}

c_moreced = { 
	1000.1.1 = { change_development_level = 10 }
	1021.9.30 = {
		holder = merewood_0001 # Edmund Fairhand
	}
}

c_aesawic = { 
	1021.9.30 = {
		holder = merewood_0001 # Edmund Fairhand
	}
}

c_craghyl = {
	1021.9.30 = {
		holder = merewood_0016 # Frederic of Craghyl
	}
}

c_esckerport = {
	1021.9.30 = {
		holder = merewood_0014 # Adelar of Esckerport
	}
}

c_aldaine = { 
	1000.1.1 = { change_development_level = 9 }
	1021.9.30 = {
		liege = d_merescker
		holder = merewood_0006 # Ricard of Aldaine
	}
}

d_nortmerewood = {
	1000.1.1 = { change_development_level = 7 }
}

c_nortmerewood={
	1021.9.30 = {
		holder = merewood_0008 #Galien síl Acenaire
	}
}

c_woudbet = { 
	1010.2.8 = {
		holder = merewood_0013 # Dethard Iceguard
	}
	1017.10.17 = {
		holder = merewood_0012 # Brigida Iceguard
	}
}

c_acenaire = {
	1021.9.30 = {
		holder = merewood_0008 #Galien síl Acenaire
	}
}

c_oudmerewood = {
	1000.1.1 = { change_development_level = 7 }
	1010.2.8 = {
		holder = merewood_0013 # Dethard Iceguard
	}
	1021.9.30 = {
		holder = merewood_0014 # Adelar of Esckerport
	}
}

d_oudmerewood = {
	1010.2.8 = {
		holder = merewood_0013 # Dethard Iceguard
	}
	1017.11.17 = {
		holder = 0
	}
}

c_acenthan = {
	1000.1.1 = { change_development_level = 9 }
	1010.2.8 = {
		holder = merewood_0013 # Dethard Iceguard
	}
	1017.10.17 = {
		holder = merewood_0012 # Brigida Iceguard
	}
}

c_leighalen = {
	1011.9.30 = {
		holder = merewood_0019 # Marcan of Leighalen
	}
}
c_uanced = {
	1017.5.12 = {
		government = republic_government
		holder = merewood_0021 # Trystan "The Peasant"
	}
}