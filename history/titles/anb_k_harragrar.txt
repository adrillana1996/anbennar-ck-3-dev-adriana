c_harrabad = {
	1000.1.1 = { change_development_level = 13 }
}

c_kargazna = {
	1000.1.1 = { change_development_level = 12 }
}

c_grarrir = {
	1000.1.1 = { change_development_level = 13 }
}

c_irkorzo = {
	1000.1.1 = { change_development_level = 5 }
}

c_xhurgrum = {
	1000.1.1 = { change_development_level = 5 }
}

c_ardu = {
	1000.1.1 = { change_development_level = 6 }
}

c_windsculpt_pass = {
	1000.1.1 = { change_development_level = 6 }
}

c_krahway = {
	1000.1.1 = { change_development_level = 12 }
}

c_xornazahar = {
	1000.1.1 = { change_development_level = 6 }
}

c_dunegap = {
	1000.1.1 = { change_development_level = 5 }
}