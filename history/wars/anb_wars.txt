﻿war = {
	name = "war_1022_blue_reachman_independence"	# the name can be left out, will be generated then
	start_date = 1020.1.1
	end_date = 1024.1.1
	targeted_titles={
		k_adshaw
	}
	casus_belli = reachmman_independence_war
	attackers = { 30000 30010 30004 30009 188 30018 30019 }
	defenders = { 62 }	
}

# Totil's Claim on Bjarnrik
war = {
	name = "war_war_of_the_bears"	# the name can be left out, will be generated then
	start_date = 1021.3.8
	end_date = 1022.3.8
	targeted_titles={
		k_bjarnrik
		d_bjarnland
		c_konugrhavn
		c_torbjoldhavn
		c_nautakr
		c_burrholt
		c_fegras
	}
	casus_belli = war_of_the_bears_cb
	attackers = { 300002 } #Skali Sidaett
	defenders = { 300000 } #Gunnar Bjarnsson
	claimant = 300006 #Totil Bjarnsson
}

# War to kill the suspected cultists
war = {
	start_date = 1022.1.1
	end_date = 1024.1.1
	targeted_titles={
		d_agradalen
	}
	casus_belli = duchy_conquest_cb
	attackers = { 60011 60012 }
	defenders = { 60013 }	
}