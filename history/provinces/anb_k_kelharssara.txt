#k_kelharssara
##d_gelkalis
###c_gelkalis
665 = {		#Gelkaris

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2946 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = church_holding

    # History
}

2947 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2948 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2949 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_as_mis
664 = {		#As-mis

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2950 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2951 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2952 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_lawassar
###c_lawassar
655 = {		#lawassar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2954 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2955 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2956 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_peruskam
656 = {		#Peruskam

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2957 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2958 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_elusadul
###c_elusadul
654 = {		#Elusadul

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2962 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2963 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2964 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

###c_akurmbag
652 = {		#Akurmbag

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2965 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2966 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_kihayanbar
653 = {		#Kihayanbar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2959 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2960 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2961 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_arkasul
###c_hranapas
668 = {		#Hranapas

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2970 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2971 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2972 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_elisan
669 = {		#Elisan

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2973 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2974 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_arkasul
671 = {		#Arkasul

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2967 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2968 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2969 = {

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}